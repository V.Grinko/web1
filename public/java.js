document.addEventListener("DOMContentLoaded", function(event) {
	var btn = document.getElementById("close-form");
	btn.onclick = function() {
		document.getElementById("contact-form").style.display = "none";
		
		animate({
			duration: 500,
			timing(timeFraction) {
				return 1-timeFraction;
			},
			draw(progress) {
			    document.getElementById("contact-form").style.opacity = progress;
			}
		});
	};
	
	document.getElementById("call-btn").onclick = function() {
		document.getElementById("contact-form").style.display = "block";
		
		animate({
			duration: 500,
			timing(timeFraction) {
				return timeFraction;
			},
			draw(progress) {
			    document.getElementById("contact-form").style.opacity = progress;
			}
		});
	}
	
	var formBtn = document.getElementById('form-submit');
	formBtn.onclick = async function()
    {
        formBtn.disabled = true;

        let name = document.getElementById('form-contact').value;
        let number = document.getElementById('form-phone').value;
		let region = document.getElementById('form-region').value;
        let text = document.getElementById('form-msg').value;
        let url = 'https://example.com/url';
        let overlay = document.getElementById('load-overlay');

        const data = {name : name, number : number, region : region, message: text};
        
        overlay.style.display = "inline";

        try {
            const responce = await fetch(url, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-type' : 'application/json'
                }
            });

            const json = await responce.json();
            alert("Заявка отправлена.");
        }
        catch(e) {
            alert("Не удалось отправить заявку. Попробуйте позже.");
        }

        formBtn.disabled = false;
        overlay.style.display = "none";
    }
});

function animate({timing, draw, duration}) {

  let start = performance.now();

  requestAnimationFrame(function animate(time) {
    // timeFraction изменяется от 0 до 1
    let timeFraction = (time - start) / duration;
    if (timeFraction > 1) timeFraction = 1;

    // вычисление текущего состояния анимации
    let progress = timing(timeFraction);

    draw(progress); // отрисовать её

    if (timeFraction < 1) {
      requestAnimationFrame(animate);
    }

  });
}

